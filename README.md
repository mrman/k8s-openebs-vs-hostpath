# OpenEBS vs hostPath #

This repository is meant to provide a reproducible test suite for comparing [OpenEBS](https://openebs.io)-provisioned volumes and [`hostPath`](https://kubernetes.io/docs/concepts/storage/volumes/#hostpath) volumes. As `hostPath` on the same machine is basically the best possible case for a containerized workload, it's reasonable to expect OpenEBS (or any other robust distributed storage solution) to be slower, it's just a question of *how much*.

This repository is a companion to a [blog post written on the topic](https://vadosware.io/post/comparing-openebs-and-hostpath/).

# Results #

Here are the results:

<TODO: RESULTS>

# Prerequisites #

To make use of the code in here you're going to need to set up a few things:

0. A server to run the tests on
1. A [Kubernetes](https://kubernetes.io/) cluster
2. [OpenEBS](https://www.openebs.io/) configured with resource names `openebs-jiva-1r` and `openebs-cstor-1r`.

# Structure #

This repository contains hierarchical folders of various resource configurations that are needed for general setup or individual tests. The tests are structured as Kubernetes `Job`s that run to completion (and write output to a path on disk) -- each `Job` writes out results to a well defined directory path (normally characterizing the run) on disk, for aggregation once all tests are complete.

The Kubernetes [`Job`]() primitive is *not* used to avoid any

# Setup #

## Cluster ##

To prepare your k8s cluster for the tests, run:

```shell
$ make cluster-setup
```
Amongst other things, this will ensure that the appropriate `StorageClass` resource definitions are installed on your cluster (we're assuming that OpenEBS is already functional in your setup).

## Tests ##

To run the tests:

```shell
$ make tests
```

This will inject the necessary `Job` objects into your kubernetes cluster, which will generate result data which will be copied back out into your local clone of the repository (under `output/<test run name>/data`). Note that if a jobs will only run *once* -- if you want to re-run you must clear all appropriate `Job` objects from your cluster (`make clean-test-jobs` will remove all jobs from the test namespace).

By default the test runs are named after the current date they were run (ex. `2018-01-01`) -- this means that if you run the test twice in one day, data accumulated by one test will be stored alongside another test (or overwrite, depending on how the job is written) -- most of the jobs *should* use timestamps when saving files to minimize the possibility of name collisions and unintended overwriting.

## Reports ##

**TODO** The machinery to generate reports isn't done yet

To generate reports:

```shell
$ make reports
```

The various reporting scripts will run over the available local data and roll them up into one or more reports, stored under `output/<test-run-name>/reports`.

## Future Work ##

I have no idea if the things below will ever get done (they likely won't), but they'd be cool:

- Script report gathering/creation
- More scriptability/customizability in both operation and layout of files -- I focused on simplicity, but there's no reason that this test bench can't be more generic
- Enable outputting results to S3-compatible service to allow running on more versatile setups
- Add a [Gitlab page](https://docs.gitlab.com/ee/user/project/pages/) to showcase automatically generated results
- Add a Jupyter/IPython notebook interface for playing with the data in-browser
