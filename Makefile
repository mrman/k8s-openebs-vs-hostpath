.PHONY: all cluster-setup check-tool-% \
				tests clean-tests \
# dd
				tests-dd clean-tests-dd \
				tests-1c-2gb-dd clean-tests-1c-2gb-dd tests-2c-4gb-dd clean-tests-2c-4gb-dd tests-4c-8gb-dd clean-tests-4c-8gb-dd \
				1c-2gb-dd-hostpath-1r 2c-4gb-dd-hostpath-1r 4c-8gb-dd-hostpath-1r \
				1c-2gb-dd-openebs-jiva-1r 2c-4gb-dd-openebs-jiva-1r 4c-8gb-dd-openebs-jiva-1r \
# iozone
				tests-iozone clean-tests-iozone \
				tests-1c-2gb-iozone clean-tests-1c-2gb-iozone tests-2c-4gb-iozone clean-tests-2c-4gb-iozone tests-4c-8gb-iozone clean-tests-4c-8gb-iozone \
				1c-2gb-iozone-hostpath-1r 2c-4gb-iozone-hostpath-1r 4c-8gb-iozone-hostpath-1r \
				1c-2gb-iozone-openebs-jiva-1r 2c-4gb-iozone-openebs-jiva-1r 4c-8gb-iozone-openebs-jiva-1r \
# sysbench
				tests-sysbench clean-tests-sysbench \
				tests-1c-2gb-sysbench clean-tests-1c-2gb-sysbench tests-2c-4gb-sysbench clean-tests-2c-4gb-sysbench tests-4c-8gb-sysbench clean-tests-4c-8gb-sysbench \
				1c-2gb-sysbench-hostpath-1r 2c-4gb-sysbench-hostpath-1r 4c-8gb-sysbench-hostpath-1r \
				1c-2gb-sysbench-openebs-jiva-1r 2c-4gb-sysbench-openebs-jiva-1r 4c-8gb-sysbench-openebs-jiva-1r


all: cluster-setup

export KUBECTL ?= kubectl
export K8S_NAMESPACE ?= storage-testing

# 1 hr default job timeout
export JOB_TIMEOUT_SECONDS ?= 3600

check-tool-kubectl:
	@which $(KUBECTL) &>/dev/null  || echo "'kubectl' is missing, please install it (https://kubectl.docs.kubernetes.io/)"

cluster-setup: check-tool-kubectl
	$(MAKE) -C cluster

#########
# Tests #
#########

# everything

tests: tests-dd tests-iozone
clean-tests: clean-tests-dd clean-tests-iozone

tests-dd: tests-1c-2gb-dd tests-2c-4gb-dd tests-4c-8gb-dd
clean-tests-dd: clean-tests-1c-2gb-dd clean-tests-2c-4gb-dd clean-tests-4c-8gb-dd

tests-iozone: tests-1c-2gb-iozone tests-2c-4gb-iozone tests-4c-8gb-iozone
clean-tests-iozone: clean-tests-1c-2gb-iozone clean-tests-2c-4gb-iozone clean-tests-4c-8gb-iozone

tests-sysbench: tests-1c-2gb-sysbench tests-2c-4gb-sysbench tests-4c-8gb-sysbench
clean-tests-sysbench: clean-tests-1c-2gb-sysbench clean-tests-2c-4gb-sysbench clean-tests-4c-8gb-sysbench

# 1c-2gb tests

tests-1c-2gb: tests-1c-2gb-dd tests-1c-2gb-iozone
clean-tests-1c-2gb: clean-tests-1c-2gb-dd clean-tests-1c-2gb-iozone

tests-1c-2gb-dd: 1c-2gb-dd-hostpath-1r 1c-2gb-dd-openebs-jiva-1r
clean-tests-1c-2gb-dd: clean-1c-2gb-dd-hostpath-1r clean-1c-2gb-dd-openebs-jiva-1r

tests-1c-2gb-iozone: 1c-2gb-iozone-hostpath-1r 1c-2gb-iozone-openebs-jiva-1r
clean-tests-1c-2gb-iozone: clean-1c-2gb-iozone-hostpath-1r clean-1c-2gb-iozone-openebs-jiva-1r

tests-1c-2gb-sysbench: 1c-2gb-sysbench-hostpath-1r 1c-2gb-sysbench-openebs-jiva-1r
clean-tests-1c-2gb-sysbench: clean-1c-2gb-sysbench-hostpath-1r clean-1c-2gb-sysbench-openebs-jiva-1r

# 2c-4gb tests

tests-2c-4gb: tests-2c-4gb-dd tests-2c-4gb-iozone
clean-tests-2c-4gb: clean-tests-2c-4gb-dd clean-tests-2c-4gb-iozone

tests-2c-4gb-dd: 2c-4gb-dd-hostpath-1r 2c-4gb-dd-openebs-jiva-1r
clean-tests-2c-4gb-dd: clean-2c-4gb-dd-hostpath-1r clean-2c-4gb-dd-openebs-jiva-1r

tests-2c-4gb-iozone: 2c-4gb-iozone-hostpath-1r 2c-4gb-iozone-openebs-jiva-1r
clean-tests-2c-4gb-iozone: clean-2c-4gb-iozone-hostpath-1r clean-2c-4gb-iozone-openebs-jiva-1r

tests-2c-4gb-sysbench: 2c-4gb-sysbench-hostpath-1r 2c-4gb-sysbench-openebs-jiva-1r
clean-tests-2c-4gb-sysbench: clean-2c-4gb-sysbench-hostpath-1r clean-2c-4gb-sysbench-openebs-jiva-1r

# 4c-8gb tests

tests-4c-8gb: tests-4c-8gb-dd tests-4c-8gb-iozone
clean-tests-4c-8gb: clean-tests-4c-8gb-dd clean-tests-4c-8gb-iozone

tests-4c-8gb-dd: 4c-8gb-dd-hostpath-1r 4c-8gb-dd-openebs-jiva-1r
clean-tests-4c-8gb-dd: clean-4c-8gb-dd-hostpath-1r clean-4c-8gb-dd-openebs-jiva-1r

tests-4c-8gb-iozone: 4c-8gb-iozone-hostpath-1r 4c-8gb-iozone-openebs-jiva-1r
clean-tests-4c-8gb-iozone: clean-4c-8gb-iozone-hostpath-1r clean-4c-8gb-iozone-openebs-jiva-1r

tests-4c-8gb-sysbench: 4c-8gb-sysbench-hostpath-1r 4c-8gb-sysbench-openebs-jiva-1r
clean-tests-4c-8gb-sysbench: clean-4c-8gb-sysbench-hostpath-1r clean-4c-8gb-sysbench-openebs-jiva-1r

#############
# 1C / 2GB  #
#############

1c-2gb-dd-openebs-jiva-1r:
	@echo -e "\n[info] Starting 1CPU/2GB DD OpenEBS (Jiva) tests..."
	@echo "Creating PVC (@ 1c-2gb/dd/openebs-jiva/pvc.yaml)"
	$(KUBECTL) apply -f 1c-2gb/dd/openebs-jiva/pvc.yaml
	@echo "Creating Job (@ 1c-2gb/dd/openebs-jiva/job.yaml)"
	$(KUBECTL) apply -f 1c-2gb/dd/openebs-jiva/job.yaml
	@echo "Waiting for job [job/1c-2gb-dd-openebs-jiva] completion..."
	$(KUBECTL) wait \
	--namespace $(K8S_NAMESPACE) \
	--timeout=$(JOB_TIMEOUT_SECONDS)s \
	--for=condition=complete \
	job/1c-2gb-dd-openebs-jiva

clean-1c-2gb-dd-openebs-jiva-1r:
	@echo "Cleaning out 1C/2GB DD OpenEBS Jiva job from namespace [$(K8S_NAMESPACE)]"
	@$(KUBECTL) delete job/1c-2gb-dd-openebs-jiva -n $(K8S_NAMESPACE) || echo "Job delete failed (does not exist?)"

1c-2gb-iozone-openebs-jiva-1r:
	@echo -e "\n[info] Starting 1CPU/2GB iozone OpenEBS (Jiva) tests..."
	@echo "Creating PVC (@ 1c-2gb/iozone/openebs-jiva/pvc.yaml)"
	$(KUBECTL) apply -f 1c-2gb/iozone/openebs-jiva/pvc.yaml
	@echo "Creating Job (@ 1c-2gb/iozone/openebs-jiva/job.yaml)"
	$(KUBECTL) apply -f 1c-2gb/iozone/openebs-jiva/job.yaml
	@echo "Waiting for job [job/1c-2gb-iozone-openebs-jiva] completion..."
	$(KUBECTL) wait \
	--namespace $(K8S_NAMESPACE) \
	--timeout=$(JOB_TIMEOUT_SECONDS)s \
	--for=condition=complete \
	job/1c-2gb-iozone-openebs-jiva

clean-1c-2gb-iozone-openebs-jiva-1r:
	@echo "Cleaning out 1C/2GB iozone OpenEBS Jiva job from namespace [$(K8S_NAMESPACE)]"
	@$(KUBECTL) delete job/1c-2gb-iozone-openebs-jiva -n $(K8S_NAMESPACE) || echo "Job delete failed (does not exist?)"

1c-2gb-sysbench-openebs-jiva-1r:
	@echo -e "\n[info] Starting 1CPU/2GB sysbench OpenEBS (Jiva) tests..."
	@echo "Creating PVC (@ 1c-2gb/sysbench/openebs-jiva/pvc.yaml)"
	$(KUBECTL) apply -f 1c-2gb/sysbench/openebs-jiva/pvc.yaml
	@echo "Creating Job (@ 1c-2gb/sysbench/openebs-jiva/job.yaml)"
	$(KUBECTL) apply -f 1c-2gb/sysbench/openebs-jiva/job.yaml
	@echo "Waiting for job [job/1c-2gb-sysbench-openebs-jiva] completion..."
	$(KUBECTL) wait \
	--namespace $(K8S_NAMESPACE) \
	--timeout=$(JOB_TIMEOUT_SECONDS)s \
	--for=condition=complete \
	job/1c-2gb-sysbench-openebs-jiva

clean-1c-2gb-sysbench-openebs-jiva-1r:
	@echo "Cleaning out 1C/2GB sysbench OpenEBS Jiva job from namespace [$(K8S_NAMESPACE)]"
	@$(KUBECTL) delete job/1c-2gb-sysbench-openebs-jiva -n $(K8S_NAMESPACE) || echo "Job delete failed (does not exist?)"

1c-2gb-dd-hostpath-1r:
	@echo -e "\n[info] Starting 1CPU/2GB DD Hostpath tests..."
	@echo "Creating Job (@ 1c-2gb/dd/hostpath/job.yaml)"
	$(KUBECTL) apply -f 1c-2gb/dd/hostpath/job.yaml
	@echo "Waiting for job [job/1c-2gb-dd-hostpath] completion..."
	$(KUBECTL) wait \
	--namespace $(K8S_NAMESPACE) \
	--timeout=$(JOB_TIMEOUT_SECONDS)s \
	--for=condition=complete \
	job/1c-2gb-dd-hostpath

clean-1c-2gb-dd-hostpath-1r:
	@echo "Cleaning out 1C/2GB DD Hostpath job from namespace [$(K8S_NAMESPACE)]"
	@$(KUBECTL) delete job/1c-2gb-dd-hostpath -n $(K8S_NAMESPACE) || echo "Job delete failed (does not exist?)"

1c-2gb-iozone-hostpath-1r:
	@echo -e "\n[info] Starting 1CPU/2GB iozone Hostpath tests..."
	@echo "Creating Job (@ 1c-2gb/iozone/hostpath/job.yaml)"
	$(KUBECTL) apply -f 1c-2gb/iozone/hostpath/job.yaml
	@echo "Waiting for job [job/1c-2gb-iozone-hostpath] completion..."
	$(KUBECTL) wait \
	--namespace $(K8S_NAMESPACE) \
	--timeout=$(JOB_TIMEOUT_SECONDS)s \
	--for=condition=complete \
	job/1c-2gb-iozone-hostpath

clean-1c-2gb-iozone-hostpath-1r:
	@echo "Cleaning out 1C/2GB iozone Hostpath job from namespace [$(K8S_NAMESPACE)]"
	@$(KUBECTL) delete job/1c-2gb-iozone-hostpath -n $(K8S_NAMESPACE) || echo "Job delete failed (does not exist?)"

1c-2gb-sysbench-hostpath-1r:
	@echo -e "\n[info] Starting 1CPU/2GB sysbench Hostpath tests..."
	@echo "Creating Job (@ 1c-2gb/sysbench/hostpath/job.yaml)"
	$(KUBECTL) apply -f 1c-2gb/sysbench/hostpath/job.yaml
	@echo "Waiting for job [job/1c-2gb-sysbench-hostpath] completion..."
	$(KUBECTL) wait \
	--namespace $(K8S_NAMESPACE) \
	--timeout=$(JOB_TIMEOUT_SECONDS)s \
	--for=condition=complete \
	job/1c-2gb-sysbench-hostpath

clean-1c-2gb-sysbench-hostpath-1r:
	@echo "Cleaning out 1C/2GB sysbench Hostpath job from namespace [$(K8S_NAMESPACE)]"
	@$(KUBECTL) delete job/1c-2gb-sysbench-hostpath -n $(K8S_NAMESPACE) || echo "Job delete failed (does not exist?)"


############
# 2C / 4GB #
############

2c-4gb-dd-openebs-jiva-1r:
	@echo -e "\n[info] Starting 2CPU/4GB DD OpenEBS (Jiva) tests..."
	@echo "Creating PVC (@ 2c-4gb/dd/openebs-jiva/pvc.yaml)"
	$(KUBECTL) apply -f 2c-4gb/dd/openebs-jiva/pvc.yaml
	@echo "Creating Job (@ 2c-4gb/dd/openebs-jiva/job.yaml)"
	$(KUBECTL) apply -f 2c-4gb/dd/openebs-jiva/job.yaml
	@echo "Waiting for job [job/2c-4gb-dd-openebs-jiva] completion..."
	$(KUBECTL) wait \
	--namespace $(K8S_NAMESPACE) \
	--timeout=$(JOB_TIMEOUT_SECONDS)s \
	--for=condition=complete \
	job/2c-4gb-dd-openebs-jiva

clean-2c-4gb-dd-openebs-jiva-1r:
	@echo "Cleaning out 2C/4GB DD OpenEBS Jiva job from namespace [$(K8S_NAMESPACE)]"
	@$(KUBECTL) delete job/2c-4gb-dd-openebs-jiva -n $(K8S_NAMESPACE) || echo "Job delete failed (does not exist?)"

2c-4gb-iozone-openebs-jiva-1r:
	@echo -e "\n[info] Starting 2CPU/4GB iozone OpenEBS (Jiva) tests..."
	@echo "Creating PVC (@ 2c-4gb/iozone/openebs-jiva/pvc.yaml)"
	$(KUBECTL) apply -f 2c-4gb/iozone/openebs-jiva/pvc.yaml
	@echo "Creating Job (@ 2c-4gb/iozone/openebs-jiva/job.yaml)"
	$(KUBECTL) apply -f 2c-4gb/iozone/openebs-jiva/job.yaml
	@echo "Waiting for job [job/2c-4gb-iozone-openebs-jiva] completion..."
	$(KUBECTL) wait \
	--namespace $(K8S_NAMESPACE) \
	--timeout=$(JOB_TIMEOUT_SECONDS)s \
	--for=condition=complete \
	job/2c-4gb-iozone-openebs-jiva

clean-2c-4gb-iozone-openebs-jiva-1r:
	@echo "Cleaning out 2C/4GB iozone OpenEBS Jiva job from namespace [$(K8S_NAMESPACE)]"
	@$(KUBECTL) delete job/2c-4gb-iozone-openebs-jiva -n $(K8S_NAMESPACE) || echo "Job delete failed (does not exist?)"

2c-4gb-sysbench-openebs-jiva-1r:
	@echo -e "\n[info] Starting 2CPU/4GB sysbench OpenEBS (Jiva) tests..."
	@echo "Creating PVC (@ 2c-4gb/sysbench/openebs-jiva/pvc.yaml)"
	$(KUBECTL) apply -f 2c-4gb/sysbench/openebs-jiva/pvc.yaml
	@echo "Creating Job (@ 2c-4gb/sysbench/openebs-jiva/job.yaml)"
	$(KUBECTL) apply -f 2c-4gb/sysbench/openebs-jiva/job.yaml
	@echo "Waiting for job [job/2c-4gb-sysbench-openebs-jiva] completion..."
	$(KUBECTL) wait \
	--namespace $(K8S_NAMESPACE) \
	--timeout=$(JOB_TIMEOUT_SECONDS)s \
	--for=condition=complete \
	job/2c-4gb-sysbench-openebs-jiva

clean-2c-4gb-sysbench-openebs-jiva-1r:
	@echo "Cleaning out 2C/4GB sysbench OpenEBS Jiva job from namespace [$(K8S_NAMESPACE)]"
	@$(KUBECTL) delete job/2c-4gb-sysbench-openebs-jiva -n $(K8S_NAMESPACE) || echo "Job delete failed (does not exist?)"

2c-4gb-dd-hostpath-1r:
	@echo -e "\n[info] Starting 2CPU/4GB DD Hostpath tests..."
	@echo "Creating Job (@ 2c-4gb/dd/hostpath/job.yaml)"
	$(KUBECTL) apply -f 2c-4gb/dd/hostpath/job.yaml
	@echo "Waiting for job [job/2c-4gb-dd-hostpath] completion..."
	$(KUBECTL) wait \
	--namespace $(K8S_NAMESPACE) \
	--timeout=$(JOB_TIMEOUT_SECONDS)s \
	--for=condition=complete \
	job/2c-4gb-dd-hostpath

clean-2c-4gb-dd-hostpath-1r:
	@echo "Cleaning out 2C/4GB DD Hostpath job from namespace [$(K8S_NAMESPACE)]"
	@$(KUBECTL) delete job/2c-4gb-dd-hostpath -n $(K8S_NAMESPACE) || echo "Job delete failed (does not exist?)"

2c-4gb-iozone-hostpath-1r:
	@echo -e "\n[info] Starting 2CPU/4GB iozone Hostpath tests..."
	@echo "Creating Job (@ 2c-4gb/iozone/hostpath/job.yaml)"
	$(KUBECTL) apply -f 2c-4gb/iozone/hostpath/job.yaml
	@echo "Waiting for job [job/2c-4gb-iozone-hostpath] completion..."
	$(KUBECTL) wait \
	--namespace $(K8S_NAMESPACE) \
	--timeout=$(JOB_TIMEOUT_SECONDS)s \
	--for=condition=complete \
	job/2c-4gb-iozone-hostpath

clean-2c-4gb-iozone-hostpath-1r:
	@echo "Cleaning out 2C/4GB iozone Hostpath job from namespace [$(K8S_NAMESPACE)]"
	@$(KUBECTL) delete job/2c-4gb-iozone-hostpath -n $(K8S_NAMESPACE) || echo "Job delete failed (does not exist?)"

2c-4gb-sysbench-hostpath-1r:
	@echo -e "\n[info] Starting 2CPU/4GB sysbench Hostpath tests..."
	@echo "Creating Job (@ 2c-4gb/sysbench/hostpath/job.yaml)"
	$(KUBECTL) apply -f 2c-4gb/sysbench/hostpath/job.yaml
	@echo "Waiting for job [job/2c-4gb-sysbench-hostpath] completion..."
	$(KUBECTL) wait \
	--namespace $(K8S_NAMESPACE) \
	--timeout=$(JOB_TIMEOUT_SECONDS)s \
	--for=condition=complete \
	job/2c-4gb-sysbench-hostpath

clean-2c-4gb-sysbench-hostpath-1r:
	@echo "Cleaning out 2C/4GB sysbench Hostpath job from namespace [$(K8S_NAMESPACE)]"
	@$(KUBECTL) delete job/2c-4gb-sysbench-hostpath -n $(K8S_NAMESPACE) || echo "Job delete failed (does not exist?)"


############
# 4C / 8GB #
############

4c-8gb-dd-openebs-jiva-1r:
	@echo -e "\n[info] Starting 4CPU/8GB DD OpenEBS (Jiva) tests..."
	@echo "Creating PVC (@ 4c-8gb/dd/openebs-jiva/pvc.yaml)"
	$(KUBECTL) apply -f 4c-8gb/dd/openebs-jiva/pvc.yaml
	@echo "Creating Job (@ 4c-8gb/dd/openebs-jiva/job.yaml)"
	$(KUBECTL) apply -f 4c-8gb/dd/openebs-jiva/job.yaml
	@echo "Waiting for job [job/4c-8gb-dd-openebs-jiva] completion..."
	$(KUBECTL) wait \
	--namespace $(K8S_NAMESPACE) \
	--timeout=$(JOB_TIMEOUT_SECONDS)s \
	--for=condition=complete \
	job/4c-8gb-dd-openebs-jiva

clean-4c-8gb-dd-openebs-jiva-1r:
	@echo "Cleaning out 4C/8GB DD OpenEBS Jiva job from namespace [$(K8S_NAMESPACE)]"
	@$(KUBECTL) delete job/4c-8gb-dd-openebs-jiva -n $(K8S_NAMESPACE) || echo "Job delete failed (does not exist?)"

4c-8gb-dd-hostpath-1r:
	@echo -e "\n[info] Starting 4CPU/8GB DD Hostpath tests..."
	@echo "Creating Job (@ 4c-8gb/dd/hostpath/job.yaml)"
	$(KUBECTL) apply -f 4c-8gb/dd/hostpath/job.yaml
	@echo "Waiting for job [job/4c-8gb-dd-hostpath] completion..."
	$(KUBECTL) wait \
	--namespace $(K8S_NAMESPACE) \
	--timeout=$(JOB_TIMEOUT_SECONDS)s \
	--for=condition=complete \
	job/4c-8gb-dd-hostpath

clean-4c-8gb-dd-hostpath-1r:
	@echo "Cleaning out 4C/8GB DD Hostpath job from namespace [$(K8S_NAMESPACE)]"
	@$(KUBECTL) delete job/4c-8gb-dd-hostpath -n $(K8S_NAMESPACE) || echo "Job delete failed (does not exist?)"

4c-8gb-iozone-openebs-jiva-1r:
	@echo -e "\n[info] Starting 4CPU/8GB iozone OpenEBS (Jiva) tests..."
	@echo "Creating PVC (@ 4c-8gb/iozone/openebs-jiva/pvc.yaml)"
	$(KUBECTL) apply -f 4c-8gb/iozone/openebs-jiva/pvc.yaml
	@echo "Creating Job (@ 4c-8gb/iozone/openebs-jiva/job.yaml)"
	$(KUBECTL) apply -f 4c-8gb/iozone/openebs-jiva/job.yaml
	@echo "Waiting for job [job/4c-8gb-iozone-openebs-jiva] completion..."
	$(KUBECTL) wait \
	--namespace $(K8S_NAMESPACE) \
	--timeout=$(JOB_TIMEOUT_SECONDS)s \
	--for=condition=complete \
	job/4c-8gb-iozone-openebs-jiva

clean-4c-8gb-iozone-openebs-jiva-1r:
	@echo "Cleaning out 4C/8GB iozone OpenEBS Jiva job from namespace [$(K8S_NAMESPACE)]"
	@$(KUBECTL) delete job/4c-8gb-iozone-openebs-jiva -n $(K8S_NAMESPACE) || echo "Job delete failed (does not exist?)"

4c-8gb-sysbench-openebs-jiva-1r:
	@echo -e "\n[info] Starting 4CPU/8GB sysbench OpenEBS (Jiva) tests..."
	@echo "Creating PVC (@ 4c-8gb/sysbench/openebs-jiva/pvc.yaml)"
	$(KUBECTL) apply -f 4c-8gb/sysbench/openebs-jiva/pvc.yaml
	@echo "Creating Job (@ 4c-8gb/sysbench/openebs-jiva/job.yaml)"
	$(KUBECTL) apply -f 4c-8gb/sysbench/openebs-jiva/job.yaml
	@echo "Waiting for job [job/4c-8gb-sysbench-openebs-jiva] completion..."
	$(KUBECTL) wait \
	--namespace $(K8S_NAMESPACE) \
	--timeout=$(JOB_TIMEOUT_SECONDS)s \
	--for=condition=complete \
	job/4c-8gb-sysbench-openebs-jiva

clean-4c-8gb-sysbench-openebs-jiva-1r:
	@echo "Cleaning out 4C/8GB sysbench OpenEBS Jiva job from namespace [$(K8S_NAMESPACE)]"
	@$(KUBECTL) delete job/4c-8gb-sysbench-openebs-jiva -n $(K8S_NAMESPACE) || echo "Job delete failed (does not exist?)"

4c-8gb-iozone-hostpath-1r:
	@echo -e "\n[info] Starting 4CPU/8GB iozone Hostpath tests..."
	@echo "Creating Job (@ 4c-8gb/iozone/hostpath/job.yaml)"
	$(KUBECTL) apply -f 4c-8gb/iozone/hostpath/job.yaml
	@echo "Waiting for job [job/4c-8gb-iozone-hostpath] completion..."
	$(KUBECTL) wait \
	--namespace $(K8S_NAMESPACE) \
	--timeout=$(JOB_TIMEOUT_SECONDS)s \
	--for=condition=complete \
	job/4c-8gb-iozone-hostpath

clean-4c-8gb-iozone-hostpath-1r:
	@echo "Cleaning out 4C/8GB iozone Hostpath job from namespace [$(K8S_NAMESPACE)]"
	@$(KUBECTL) delete job/4c-8gb-iozone-hostpath -n $(K8S_NAMESPACE) || echo "Job delete failed (does not exist?)"

4c-8gb-sysbench-hostpath-1r:
	@echo -e "\n[info] Starting 4CPU/8GB sysbench Hostpath tests..."
	@echo "Creating Job (@ 4c-8gb/sysbench/hostpath/job.yaml)"
	$(KUBECTL) apply -f 4c-8gb/sysbench/hostpath/job.yaml
	@echo "Waiting for job [job/4c-8gb-sysbench-hostpath] completion..."
	$(KUBECTL) wait \
	--namespace $(K8S_NAMESPACE) \
	--timeout=$(JOB_TIMEOUT_SECONDS)s \
	--for=condition=complete \
	job/4c-8gb-sysbench-hostpath

clean-4c-8gb-sysbench-hostpath-1r:
	@echo "Cleaning out 4C/8GB sysbench Hostpath job from namespace [$(K8S_NAMESPACE)]"
	@$(KUBECTL) delete job/4c-8gb-sysbench-hostpath -n $(K8S_NAMESPACE) || echo "Job delete failed (does not exist?)"
